# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete("a-z")
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  length = str.length
  middle = length / 2
  length.odd? ? str[middle] : str[(middle - 1), 2]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.chars.count { |letter| VOWELS.include?(letter) }
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  arr.reduce("") do |string, el|
    string.empty? ? el : string + separator + el
  end
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.reduce('') do |result, char|
    result.length.even? ? result + char.downcase : result + char.upcase
  end
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split
  words.map! { |word| word.length < 5 ? word : word.reverse }
  words.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).to_a.map do |num|
    if (num % 15).zero?
      "fizzbuzz"
    elsif (num % 5).zero?
      "buzz"
    elsif (num % 3).zero?
      "fizz"
    else
      num
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reduce([]) { |reversed, el| reversed.unshift(el) }
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  factors(num).size == 2
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select { |factor| (num % factor).zero? }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |factor| prime?(factor) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).size
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  arr.count(&:even?) == 1 ? arr.detect(&:even?) : arr.detect(&:odd?)
end
